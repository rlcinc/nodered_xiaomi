var injectCss = function (url, callback) {
	//console.log('inject css', url);
	var css = document.createElement('link');
	css.id = url;
	css.setAttribute('href', url);
	css.setAttribute('rel', 'stylesheet');
	document.getElementsByTagName("head")[0].appendChild(css);
	if (typeof(callback)!='undefined') callback();
}

/*var injectScript = function (url, callback, scriptCode, srcType) {
	//console.log('inject JS', url);
	var script = document.createElement("script");
	script.type = (typeof(srcType) === 'undefined') ? "text/javascript" : srcType;
	script.id = url;
	script.async = true;
	if (typeof(scriptCode) === 'undefined') script.src = url;
	
	if (typeof callback != 'undefined' && typeof callback != null) {
		if (script.readyState) { //IE
			script.onreadystatechange = function () {
				if (script.readyState == "loaded" || script.readyState == "complete") {
					script.onreadystatechange = null;
					callback();
				}
			};
		} else { //Others
			script.onload = callback;
		}
	}
	document.getElementsByTagName("head")[0].appendChild(script);
	if (typeof(scriptCode) !== 'undefined') {
		script.appendChild(document.createTextNode(scriptCode));
	}
}*/