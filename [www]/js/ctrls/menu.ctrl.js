smartHome.registerModule.controller('menu.ctrl', function($scope, $rootScope) {
	$scope.menu = $rootScope.serverConfig.webCfg.menu;

	$rootScope.$on('keypress', function(evt, obj, key) {
		$scope.$apply(function() {
			$scope.key = key;
			if ($scope.key == 27 && $rootScope.isShowSidebar) $rootScope.isShowSidebar = false;
		});
	})

	$scope.changeTab = function(value) {
		$rootScope.isShowSidebar = false;
		$rootScope.$isShowEditDialog = false;
		$rootScope.clientConfig.activeTab = value;
		$rootScope._saveClientConfig();
	}

	if (!$rootScope.clientConfig.activeTab) $rootScope.clientConfig.activeTab = {index:0};

});