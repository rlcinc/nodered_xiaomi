﻿smartHome.registerModule.controller('dashboard.devicecard.ctrl', function ($scope, $rootScope, $http) {
	$scope.parentCtrl = $scope.$parent.$parent.$parent.$parent.$parent.$parent;

	$scope.setNotification = function(device) {
		device._isSendNotification = !device._isSendNotification; 
		$scope.parentCtrl.curItem = device; 
		$scope.parentCtrl.updateCfg(true);
	}
});