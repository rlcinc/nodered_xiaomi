﻿smartHome.registerModule.controller('dashboard.devicecard.ctrl', function ($scope, $rootScope, $http) {
	$scope.isNeedShow = false;
    $scope.parentCtrl = $scope.$parent.$parent.$parent.$parent.$parent;
	$scope.fieldOrder = ['cron','comment','isActive'];
	$scope.codeMirror = {_content : "", _fileName : 'cron.js'};
    $scope.cronData = [];

	//console.log('data', $scope.data, $scope.parentCtrl);

	var init = function() {
		
		$rootScope.ws.$emit('getDeviceEventCode', {sid : $scope.data.data.sid, type:"cron"});
	}

	$rootScope.ws.$on('$message', function (response) {
		$scope.$apply(function () {
			switch (response.event) {
				case 'getDeviceEventCode' : $scope.cronData = response.data; break;
			}
		});
	});

    $scope.delRecord = function(ind) {
		var newItem = {};
		$scope.cronData.splice(ind,1);
		console.log(ind, $scope.cronData);
	}

	$scope.newRecord = function() {
		var newItem = {};
		$scope.cronData.push(newItem);
		//$scope.curItem = item;
	}

	$scope.editItem = function(item) {
		$scope.curItem = item;
		$scope.isNeedShow = true;
		console.log($scope.curItem);
		$scope.codeMirror._content = item.code;
	}

	init();

});