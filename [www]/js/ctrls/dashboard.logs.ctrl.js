smartHome.registerModule.controller('dashboard.logs.ctrl', function($scope, $rootScope, $filter) {

	$scope.prepareInputRawData = function(data) {
		var result = data.split('\n');
		//console.log(result);
		for (var i=0; i< result.length; i++) {
			//console.log(result[i]);
			try {
				result[i] = JSON.parse(result[i]);
			} catch(ex) {
				console.log(result[i]);
			}
		}
		return result;
	}


	$scope.parentCtrl = $scope.$parent.$parent.$parent.$parent.$parent;
	$scope.logData = $scope.prepareInputRawData($scope.data.data);
	$scope.logType = $scope.data.logType;

	//console.log($scope.logData);
	$scope.getProperty = function(param, isNeedDivide) {
		var result = []
		for (var i = 0; i < $scope.logData.length; i++) {
			//var date = new Date($scope.logData[i].lastEventDate);
			result.push($scope.logData[i][param] * 1);
		}
		return result;
	}

	$scope.getDataByIndex = function(key) {
		var dtFormat = $rootScope._translateKey('date.format');
		return ($scope.logData[key]) ? $filter('date')($scope.logData[key].lastEventDate, dtFormat) : '';
	}

	$scope.prepareDataByType = function() {
		function isReportExist(type) {
			var name = 'deviceList.log.reportType.' + type;
			var report = $rootScope._translateKey(name);
			if (report === name) return [];
			return JSON.parse(report);
		}

		var props = isReportExist($scope.logType);

		$scope.reportOpt = { series : [], seriesDesc : []};
		for (var i = 0; i < props.length; i++) {
			$scope.reportOpt.series.push({
				name: props[i].label,
				data: $scope.getProperty(props[i].name)
			});
			$scope.reportOpt.seriesDesc.push(props[i].legend);
		}
		$scope.reportOpt.yTitle = props.reduce(function(a, b) {return a + ["", " / "][+!!a.length] + b.legend;}, "");
	}

	$scope.prepareDataByType();

	$scope.chartConfig = {
		noData : {
			style : { "fontSize": "45px", "fontWeight": "bold", "color": "#666666" }
		},
		lang: {
        	noData: $rootScope._translateKey('deviceList.log.noData'),
	    },
		title: {
			text: ''
		},
		subtitle: {
			text: ''
		},
		chart: {
			type: 'areaspline',
			backgroundColor: 'transparent',
			zoomType: 'xy',
			resetZoomButton: {
				position: {
					//align: 'right', // by default
					//verticalAlign: 'top', // by default
					x: -10,
					y: 10
				},
				theme: {
					fill: 'white',
					stroke: 'silver',
					r: 0,
					states: {
						hover: {
							fill: '#41739D',
							style: {
								color: 'white'
							}
						}
					}
				}
			}
		},
		navigator: {
			xAxis: {
				labels: {
					formatter: function() {
						//console.log('navigator', this);
						return $scope.getDataByIndex(this.value);
					}
				}
			},
			enabled: true,
			series: {
				marker: {
					enabled: false
				},
				data: []
			}
		},
		xAxis: {
			labels: {
				formatter: function() {
					return $scope.getDataByIndex(this.value);
				}
			},
			title: {
				text: ''
			}
		},
		yAxis: {
			title: {
				text: $scope.reportOpt.yTitle
			},
			labels: {
				formatter: function() {
					return this.value;
				}
			}
		},
		plotOptions: {
			area: {
				marker: {
					enabled: false,
					symbol: 'circle',
					radius: 2,
					states: {
						hover: {
							enabled: true
						}
					}
				}
			}
		},
		tooltip: {
			crosshairs: [{
				width: 1,
				dashStyle: 'dash',
				color: '#898989'
			}, {
				width: 1,
				dashStyle: 'dash',
				color: '#898989'
			}],
			formatter: function() {
				// TDB. Need use additional template for tooltips

				//console.log(this);

				var s = '<b>' + $scope.getDataByIndex(this.x) + '</b>';
				for (var i = 0; i < this.points.length; i++) {
					//console.log(this.points[i].series.chart.options.seriesDesc[i]);
					s += '<br/>' + this.points[i].series.name + ': ' + this.points[i].y + ' ' + this.points[i].series.chart.options.seriesDesc[i];
				}
				return s;// + JSON.stringify($scope.logData[this.x]);
			},
			borderWidth: 1,
			borderRadius: 5,
			borderColor: '#a4a4a4',
			shadow: false,
			useHTML: true,
			percentageDecimals: 2,
			backgroundColor: "rgba(255,255,255,.7)",
			style: {
				padding: 0
			},
			shared: true
		},

		rangeSelector: {
			enabled: false,
			selected: 0
		},

		series: $scope.reportOpt.series,
		seriesDesc : $scope.reportOpt.seriesDesc,
		startWithLastItems : true
	}
});