smartHome.registerModule.controller('fsbrowser.ctrl', function($scope, $rootScope, $http, $window) {
	$scope.editor = null;
	$scope.needSave = false;
	$scope.curFile;
	$rootScope.listFiles = null;

	var init = function() {
		$scope.filesList();
		
		$scope.editor = new window.CodeMirror(function(cm_el) {
			document.getElementById('editorExt').append(cm_el);
		}, $scope.editorOpt);


		CodeMirror.commands.save = function(cm) {
			$scope.saveFile();
		};
		CodeMirror.keyMap.default["Ctrl-S"] = "save";

		CodeMirror.commands.jsbeautifier = function(cm) {
			$scope.jsbeautifier();
		};
		CodeMirror.keyMap.default["Ctrl-B"] = "jsbeautifier";

		CodeMirror.commands.fileslist = function(cm) {
			$scope.filesList();
		};
		CodeMirror.keyMap.default["Ctrl-L"] = "fileslist";

		$scope.editor.on("change", function(cm, change) {
			console.log('onChange', change);
			$scope.$evalAsync(function() {
				$scope.needSave = (change.origin != "setValue");
				$scope.fileSize = $scope.editor.getValue().length;
				//console.log('needSave', $scope.needSave);
			});
		});
	}

	$scope.loadFile = function(file) {
		$scope.curFile = file;
		$rootScope.message = 'Loaging file: ' + $scope.curFile;

		file = file.replace('.gz', '');
		$http({
			url: file + '?v=' + Math.round(new Date() / 1000),
			method: 'GET',
			transformResponse: null
		}).then(function(response) {
			try {
				detectEditorMode(file);
			} catch (exp) {
				console.log('please check plugin path');
			}
			$scope.editor.setValue(response.data);
			if ($scope.curFile.startsWith('/logs/')) {
				setTimeout(function() {
					if ($scope.curFile.startsWith('/logs/')) $scope.loadFile($scope.curFile);
				}, 15000);
			}

		}, function(response) {
			$rootScope.action = {
				type: "error",
				text: "Can't load file:" + file
			};
		});
	}

	function detectEditorMode(file) {
		var val = file,
			m, mode, spec;
		if (m = /.+\.([^.]+)$/.exec(val)) {
			var info = CodeMirror.findModeByExtension(m[1]);
			if (info) {
				mode = info.mode;
				spec = info.mime;
			}
		} else if (/\//.test(val)) {
			var info = CodeMirror.findModeByMIME(val);
			if (info) {
				mode = info.mode;
				spec = val;
			}
		} else {
			mode = spec = val;
		}
		if (mode) {
			$scope.editor.setOption("mode", spec);
			window.CodeMirror.autoLoadMode($scope.editor, mode);
			//console.log(mode);
		} else {
			console.log("Could not find a mode corresponding to " + val);
		}
	}

	$scope.editorOpt = {
		styleActiveLine: true,
		highlightSelectionMatches: {
			showToken: /\w/,
			annotateScrollbar: true
		},
		styleActiveLine: true,
		matchBrackets: true,
		indentWithTabs: true,
		showTrailingSpace: true,
		autofocus: true,
		foldGutter: true,
		height: "auto",
		theme: 'default',
		lineWrapping: false,
		scrollbarStyle: 'native',
		foldGutter: true,
		gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
		extraKeys: {
			"Ctrl-Space": "autocomplete"
		},
		matchBrackets: true,
		autoCloseBrackets: true,
		matchTags: {
			bothTags: true
		},
		autoCloseTags: true,
		lineNumbers: true,
		tabSize: 4,
		indentUnit: 4
	};

	$scope.filesList = function() {
		$rootScope.ws.$emit('UI.listFiles');
	}

	$scope.deleteFile = function(file) {
		$http({
			url: file + '?v=' + Math.round(new Date() / 1000),
			method: 'DELETE',
		}).then(function(response) {
			
			$rootScope.action = {
				type: "success",
				text: file + " - has been deleted."
			};
			$scope.filesList();
		}, function(response) {
			$rootScope.action = {
				type: "error",
				text: "Can't delete file " + file
			};
		});
	}

	$scope.uploadFile = function(file) {
		$scope.saveFile(file);
	};

	$scope.checkJSSyntax = function(file) {
		if (!(file.endsWith('.js') || file.endsWith('.js.gz'))) return true;
		//console.log('check js syntax ' + file);
		try {
			var code = "(function () { try { " + $scope.editor.getValue() + " } catch(e) { console.log('CHECK JS',e); } })();\n//# sourceURL=jscheck.js";
			var script = document.createElement("script");
			script.type = "text/javascript";
			script.async = false;
			document.getElementsByTagName("head")[0].appendChild(script);
			script.appendChild(document.createTextNode(code));
			eval(code);
		} catch(exp) {
			if (exp instanceof SyntaxError)	{
				//console.log('Syntax error');
				console.log(exp.message, exp.name, exp.fileName, 'lineNumber :' + exp.lineNumber, 'colimnNumber:' + exp.columnNumber, exp.stack);
				$rootScope.action = {
					type: "error",
					text: exp.toString() + '.Additional information in Developer console. Press F12.'
				};
				return false;
			}
		}
		return true;
	}

	$scope.saveFile = function(file) {
		var fileOfBlob;
		var payload = new FormData();
		if (typeof(file) == 'undefined') {
			if (!$scope.checkJSSyntax($scope.curFile)) return;
			var zippedResult = pako.gzip($scope.editor.getValue(), {
				to: "Uint8Array"
			});
			var convertedZipped = zippedResult.buffer;

			var content = $scope.curFile.endsWith('.gz') ? convertedZipped : $scope.editor.getValue();

			var blob = new Blob([content], {
				type: 'application/octet-stream'
			});
		}
		fileOfBlob = typeof(file) != 'undefined' ? file : new File([blob], $scope.curFile);
		if (fileOfBlob.name.length == 0) return;
		$rootScope.message = 'Saving file: ' + fileOfBlob.name;

		payload.append("upload", fileOfBlob);
		$http({
			url: fileOfBlob.name.startsWith('/') ? fileOfBlob.name : '/' + fileOfBlob.name,
			method: 'PUT',
			data: payload,
			headers: {
				'Content-Type': undefined
			},
			transformRequest: angular.identity
		}).then(function(response) {
			$scope.filesList();
			$scope.needSave = false;
			$rootScope.action = {
				type: "success",
				text: fileOfBlob.name + " - has been saved."
			};

		}, function(response) {
			$scope.needSave = true;
			$rootScope.action = {
				type: "error",
				text: "Can't save file " + fileOfBlob.name
			};
		});
	}

	$scope.jsbeautifier = function() {
		var the = {
			use_codemirror: true,
			beautify_in_progress: false,
			editor: $scope.editor
		};
		$window.beautify(the);
	}

	init();
});