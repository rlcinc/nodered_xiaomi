smartHome.registerModule.controller('devicelist.ctrl', function ($scope, $rootScope, $http) {
	$scope.currentGroup = '_withoutGrouping';
	$scope.groupBy = [{name : "_withoutGrouping", label : 'deviceList.withoutGrouping'}, {name : "_type", label : 'deviceList._type'}, {name : "_name", label : "deviceList._name"}, {name : "_zone", label : "deviceList._zone"}];

	$scope.fieldOrder = ["_sid", "_type", "_hasParent", "voltageInPercentage", "voltage", "_name", "_zone","lastEventDate","_typeOfEvent", "_isActive", "_ip", "image"];
	$scope.curItem = {};
	$scope.eventAction ={};
	$scope.codeMirror = {_content : "", _fileName : 'event.js'};
	
	$scope.devices = Object.keys($rootScope.cfg).map(function(key) {
	    return $rootScope.cfg[key];
	});

	$scope.saveDeviceInfo = function(isNeedClose) {
		$scope.errorMsg = null;
		$scope.isNeedClose = isNeedClose;
		$scope.updateCfg(isNeedClose);
		$rootScope.$isShowEditDialog = isNeedClose && $scope.saveEvent();
	}

	$scope.updateCfg = function(isNeedClose) {
		_updateDataInItem();
		$rootScope.ws.$emit('UI.setDevice', $rootScope.cleanData($rootScope.cfg[$scope.curItem._sid]));
	}

	$scope.editItem = function(elId) {
		
		$scope.codeMirror._content = '';
		$scope.codeMirror._cmd = 'init';
		$scope.curItem._curEvent = null;

		//$scope.getDeviceAction(elId,'Info');
		var tmp = angular.fromJson(angular.toJson($rootScope.cfg[elId]));
		$scope.curItem = tmp;
		console.log('editItem', $scope.curItem);
		$rootScope.$isShowEditDialog = true;
	}

	$scope.getDeviceAction = function(elId, action) {
		$rootScope.ws.$emit('UI.getDevice' + action, {_sid : elId});
		$scope.logData = null;
	}

	$scope.changeEvent = function(item, $event) {
		console.log('asdadss', item);
		$scope.curItem._curEvent = item.name;
		$scope.eventAction = {_sid : $scope.curItem._sid, type:"event", event : item.name};
		$scope.codeMirror._cmd = 'init'; 
		$scope.codeMirror._content = '';
		$rootScope.ws.$emit('UI.getDeviceEventCode', $scope.eventAction);

	}

	$scope.changeGroupBy = function(item, $event) {
		$scope.currentGroup = item.name;
	}

	$scope.saveEvent = function() {
		if (typeof($scope.eventAction._sid)=='undefined') return false;
		$scope.eventAction.code = $scope.codeMirror._content;
		//console.log($scope.eventAction, $scope.curItem);
		$rootScope.ws.$emit('UI.setDeviceEventCode', $scope.eventAction);
		$scope.codeMirror._cmd = 'init';
		return true;
	}

	$rootScope.ws.$on('$message', function (response) {
		$scope.$apply(function () {
			switch (response.event) {
				case 'UI.getDeviceEventCode' : $scope.getDeviceEventCode(response.data);break;
				case 'UI.getDeviceLog': $rootScope.$isShowLogDialog = true; $scope.logData = response.data.data; $scope.curItem = $rootScope.cfg[response.data._sid]; break;
				case 'UI.setDeviceEventCode' : $scope.setDeviceEventCode(response.data); break;
			}
		});
	});

	/*
		PRIVATE METHODS
	*/  	

	$scope.sendUICmd = function(device, cmd) {
		console.log(device, cmd);

		var params = {
			volume : 1,
			ringtone : 1	
		}
		$rootScope.ws.$emit("UI.sendCmdToDevice", {_sid : device._sid, cmd: cmd.cmd, params : params});
	}

	$scope.getDeviceEventCode = function(data) {
		$scope.eventAction = data.actions[0]; 
		$scope.codeMirror._cmd = 'init'; 
		$scope.codeMirror._content = $scope.eventAction.code; 
	}

	$scope.setDeviceEventCode = function(data) {
		console.log(data.message);
		if (data.result === true) {
			$rootScope.$isShowEditDialog = !$scope.isNeedClose;
			$scope.errorMsg = null;
		} else {
			$scope.errorMsg = data.message;
		}
	}

	_updateDataInItem = function() {
		for (var key in $scope.curItem) {
			if (key.startsWith('$$')) continue;
			//if (key.startsWith('_') && (key!='_name' || key!='_zone')) continue;
			$rootScope.cfg[$scope.curItem._sid][key] = $scope.curItem[key];
			console.log($scope.curItem[key]);
		}
	}




});