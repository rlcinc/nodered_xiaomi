﻿smartHome.registerModule.controller('config.client.ctrl', function ($scope, $rootScope, $http, LOCALES) {
    $scope.languages = [];
	//$scope.codeMirror = {content : "", fileName : 'test.js'};

	var init = function(){
		for (var key in LOCALES.locales) {
			$scope.languages.push({name: key, label:LOCALES.locales[key]});
		}
	}

	$scope.restart = function() {
		$rootScope.ws.$emit('UI.restart', {});
	}

	$scope.changeLang = function(item, $event) {
		$rootScope.clientConfig.lang = item.name;
		$rootScope._changeLanguage(item.name);
	}

	init();
});