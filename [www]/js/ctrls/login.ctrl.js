smartHome.registerModule.controller('login.ctrl', function ($scope, $rootScope, $http,$window) {

    $scope.loadCfg = function () {
		
	    if ($rootScope.clientConfig == null) return;
    	if ($scope.wwwpass!== null)	$http.defaults.headers.common.Authorization = 'Basic ' + btoa($scope.username + ":" + 	$scope.wwwpass);
		
    	$http.get("/config.json")
			.then(function (response) {
				$rootScope.serverConfig = response.data;
				console.log($rootScope.serverConfig);
				console.log($http.defaults.headers.common.Authorization, $rootScope.serverConfig);

				var connectUrl = $rootScope.serverConfig.webCfg.wsUrl.replace('{0}', $http.defaults.headers.common.Authorization).replace('{bindIP}', $window.location.hostname).replace('{httpPort}', $window.location.port);
				$rootScope.initWS(connectUrl, $scope.successLogin);
				$rootScope.isDev = ($rootScope.serverConfig.webCfg.ver.toLowerCase().indexOf('dev')!=-1);
				if ($rootScope.clientConfig.isShowLog) $rootScope.readLog();
			}, function (response) {
				if (response.status == 401) {
	            	$rootScope.action = {
						type: "error",
						text: response.data
					};

				}
			});
    }

	$scope.successLogin = function() {
		$rootScope.isLoginSuccess = true;
		$rootScope.clientConfig.username = $scope.username;
		$rootScope.clientConfig.pass = $scope.wwwpass;
		$rootScope._saveClientConfig();
	}	

	$scope.logout = function() {
		$rootScope.clientConfig.username = undefined;
		$rootScope.clientConfig.pass = undefined;
		$rootScope._saveClientConfig();
	}
	

	if (typeof($rootScope.clientConfig.username) !='undefined') {
		$scope.wwwpass = $rootScope.clientConfig.username;
		$scope.username = $rootScope.clientConfig.pass;
		$scope.loadCfg();
	}
});