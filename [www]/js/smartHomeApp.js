'use strict';

var app = angular.module('smartHomeApp', ['ngWebsocket', 'ui.directives', 'pascalprecht.translate', 'ngSanitize','angular.filter','highcharts-ng']).constant('LOCALES', { //'tmh.dynamicLocale',
    'locales': {
        'ru_RU': 'Русский',
        'en_US': 'English'
    },
    'preferredLocale': 'en_US'
});

app.config(['$controllerProvider','$windowProvider', '$translateProvider', 'LOCALES', function($controllerProvider, $windowProvider, $translateProvider, LOCALES) { //'tmhDynamicLocaleProvider',
    'use strict';
    $windowProvider.$get().smartHome = $windowProvider.$get().smartHome || {};
    $windowProvider.$get().smartHome.registerModule = {
        controller: $controllerProvider.register
    };
	$translateProvider.useSanitizeValueStrategy(null);
	$translateProvider.useStaticFilesLoader({
        prefix: '/i18n/locale-',// path to translations files
        suffix: '.json'// suffix, currently- extension of the translations
    });
    $translateProvider.preferredLanguage(LOCALES.preferredLocale);// is applied on first load
}]);


app.controller('smartHomeCtrl', function ($scope, $http, $rootScope, $window, $element, $compile, $websocket, $translate) {
	$rootScope.clientConfig = {};
	$rootScope.svgurl = '/icons/{0}-sprite/svg/symbols.svg#{1}';
	$rootScope.action = {};
	$scope.isLoaded = false;

	$scope.isLoading = function () {
		var result = $http.pendingRequests.length > 0;
		if (result == false && $scope.isLoaded == true) $rootScope.message = "";
		return result;
	};

	
	$rootScope.initWS = function(wsUrl, successFunc) {
		$rootScope.ws = $websocket.$new({"url": wsUrl, "reconnect": true, "reconnectInterval": 500});


		$rootScope.ws.$on('$error', function (error) {
			console.log('Error in server ' + $rootScope.ws.$status(), error.target.readyState);
			$scope.$apply(function () {
            	$rootScope.action = {
					type: "error",
					text: 'ERROR : ' + (error.target.readyState == 3 ? 'Connection closed by client.' : error.target.readyState)
				};
				$rootScope.ws.$close();
        	});
			
		});

		$rootScope.ws.$on('$open', function (event) {
		    $rootScope.ws.$emit('UI.readState');
			successFunc();
		});

		$rootScope.ws.$on('$close', function (event) {
			console.log('Connection is closed ', event, $rootScope.ws.$status(), $rootScope.ws.$$config);
		});


		$rootScope.ws.$on('$message', function (response) {
            console.log("DATA FROM SERVER", response);
			$rootScope.$apply(function () {
				switch (response.event) {
					case 'UI.readState' : $rootScope.cfg = response.data; break;
					case 'UI.listFiles' : $rootScope.listFiles = [response.data]; break;
					case 'updateState' : $rootScope._updateDataInItem($rootScope.cfg[response.data._sid], response.data); break;
					case 'addLog' : $rootScope.$log += response.data; break;
					default : {
						if (response.event.startsWith('device.')) {
							$rootScope._updateDataInItem($rootScope.cfg[response.data._sid], response.data); break;
						}
					}
				}
			});
		});
    }

	/*$scope.refreshState = function() {
		$rotScope.ws.$emit('UI.readState');
	}*/

	$rootScope.cleanData = function(data) {
		return angular.fromJson(angular.toJson(data));
	}
	
	$rootScope._updateDataInItem = function(oldValue, newValue) {
		for (var key in newValue) {
			if (key.startsWith('$$')) continue;
			oldValue[key] = newValue[key];
			$rootScope.getDeviceUrl(oldValue);
		}
	}


	$rootScope.getDeviceUrl = function(item) {	
		var url = $rootScope.serverConfig.webCfg.imgUrl.replace('{type}', item._type);
		//console.log('url', item, url);
		return item._type ? url : undefined;
	}

	$rootScope._loadClientConfig = function() {
		var config = localStorage.getItem('RLC');
		if (config != null)	$rootScope.clientConfig = JSON.parse(config);
		if (typeof($rootScope.clientConfig.lang) == 'undefined') $rootScope.clientConfig.lang = "en_US";
		$rootScope._changeLanguage($rootScope.clientConfig.lang);
		$rootScope.isDev = $rootScope.clientConfig.isDev;
	}

   	$rootScope._saveClientConfig = function() {
		localStorage.setItem('RLC', JSON.stringify($rootScope.clientConfig));
	}

	$rootScope._changeLanguage = function(locale) {
		$translate.use(locale);
    };

	$rootScope._translateKey = function(key) {
		return $translate.instant(key);
	}

    $rootScope.$on('$translateChangeSuccess', function (event, data) {
		document.documentElement.setAttribute('lang', data.language);// sets "lang" attribute to html
		$rootScope._saveClientConfig();
    });

	$rootScope.readLog = function() {
		if (!$rootScope.serverConfig) return;
	    var startPos = $rootScope.serverConfig.logFile.indexOf($rootScope.serverConfig.webDir) + $rootScope.serverConfig.webDir.length;
		var logUrl = $rootScope.serverConfig.logFile.substr(startPos);

    	$http.get(logUrl)
			.then(function (response) {
				$rootScope.$log = response.data;
			}, function (response) {
				if (response.status == 401) {
	            	$rootScope.action = {
						type: "error",
						text: response.data
					};
				}
			});
	}

    $scope.init = function() {
		$rootScope._loadClientConfig()		
		$scope.isLoaded = true;
    }

	$scope.init();
});

angular.bootstrap(document, ['smartHomeApp']);