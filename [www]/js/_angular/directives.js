angular.module('ui.directives', [])
	.run(["$templateCache", function($templateCache) {
		"use strict";
		$templateCache.put("sldsSvgIcon.tpl.html", '<svg aria-hidden="true" class="{{addclasses}}" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid meet" ><use xlink:href=""></use></svg>');
		$templateCache.put("sldsMessage.tpl.html", '<div class="slds-notify_container" ng-show="(text.length > 0)"><div class="slds-notify slds-notify--alert slds-theme--{{type}} slds-theme--alert-texture" role="alert"><button ng-click="text = null" class="slds-button slds-notify__close slds-button--icon-inverse" title="Close"><slds-Svg-Icon sprite="\'utility\'" icon="\'close\'" addclasses="\'slds-button__icon\'"></slds-Svg-Icon><span class="slds-assistive-text">Close</span></button><h2>{{text}}</h2></div></div>');
		$templateCache.put("sldsPicklist.tpl.html", '<div ng-init="isOpened = false" class="slds-picklist slds-dropdown-trigger slds-dropdown-trigger--click {{isOpened ? \'slds-is-open\' : \'\'}}" ng-click="isOpened = !isOpened">\n  <div class="slds-form-element">\n    <label class="slds-form-element__label" style="white-space: nowrap; padding-right:35px" for="text-input-01">{{caption | translate}}</label>\n    <div class="slds-form-element__control slds-input-has-icon slds-input-has-icon--right slds-picklist__input">\n      <input type="search" id="text-input-01" class="slds-lookup__search-input slds-input" placeholder="{{(!curItem.name ? placeholder : curItem.label) | translate}}" aria-owns="option-list-01" role="combobox" aria-activedescendant="" aria-expanded="false" readonly="" />\n		<slds-Svg-Icon sprite="\'utility\'" icon="\'down\'" addclasses="\'slds-button__icon--stateful slds-button__icon--right\'"></slds-Svg-Icon>\n    </div>\n  </div>\n  <div ng-mouseleave="isOpened=false" class="slds-dropdown slds-dropdown--left slds-dropdown--{{align}}" role="listbox" style="z-index:70 !important">\n    <ul id="option-list-01" class="slds-dropdown__list slds-dropdown--length-5" role="presentation">\n      <li role="presentation" ng-repeat="item in items" ng-click="$parent.curItem = item; clickAction(item, $event);">\n        <span class="slds-lookup__item-action slds-lookup__item-action--label" role="option" tabindex="0" id="listbox-option-248">\n		  <slds-Svg-Icon  ng-show="item.name == $parent.curItem.name" sprite="\'utility\'" icon="\'check\'" addclasses="\'slds-button__icon--stateful slds-button__icon--left\'"></slds-Svg-Icon>\n          <span class="slds-truncate">{{item.label | translate}}</span>\n        </span>\n      </li>\n    </ul>\n  </div>\n</div>');
		$templateCache.put("codemirror.tpl.html", '<div class="codeMirror__container"><div></div></div>');
	}])
	.directive('showCard', function($compile, $http) {
		'use strict';
		return {
			restrict: 'EA',
			scope: {
				onload:"=",
				ctrlname:"=?",
				template: '=?',
				data: '=?',
				elid: '=?'
			},
			replace: true,
			transclusion: false,
			template: "<div></div>",
			controller: function($scope, $attrs, $element, $rootScope) {
				//console.log($scope, $attrs);
				if (typeof($scope.$root.loadedTemplates) == 'undefined') $scope.$root.loadedTemplates = new Map();

				var injectScript = function(url, callback, scriptCode, srcType) {
					var script = document.createElement("script");
					script.type = (typeof(srcType) === 'undefined') ? "text/javascript" : srcType;
					script.id = url;
					script.async = true;
					if (typeof(scriptCode) === 'undefined') script.src = url;
					if (typeof callback != 'undefined' && typeof callback != null) {
						if (script.readyState) { //IE
							script.onreadystatechange = function() {
								if (script.readyState == "loaded" || script.readyState == "complete") {
									script.onreadystatechange = null;
									callback();
								}
							};
						} else { //Others
							script.onload = callback;
						}
					}
					document.getElementsByTagName("head")[0].appendChild(script);
					if (typeof(scriptCode) !== 'undefined') {
						script.appendChild(document.createTextNode(scriptCode));
					}

				}

				var injectTmpl = function(tmpl, addScriptName, elementOrId, data, onload, callback) {
					//console.log('onload', $scope, $attrs);
					onload = (onload!=null && onload!=undefined) ? onload : '';

					addScriptName = (addScriptName!=null && addScriptName!=undefined) ? addScriptName.replace('.ctrl','') : null;

					
					var devVer = $scope.$root.isDev ? '?v=' + Math.round(new Date() / 1000) : '';
					var scriptName = ('/js/ctrls/' + (addScriptName!=null ? addScriptName : tmpl) + '.ctrl.js' + devVer).toLowerCase();
					var tmplName = ('/js/tmpls/' + tmpl + '.html' + devVer).toLowerCase();

				    //console.log('tmplsName', scriptName, tmplName);

					var newScope = $scope.$new(false);
					newScope.data = (typeof(data) != 'undefined') ? data : null;

					var elem = (typeof(elementOrId) == 'string') ?
						angular.element(document.getElementById(elementOrId)) :
						elementOrId;
					if (tmpl in $scope.$root.loadedTemplates || addScriptName==='false') {
						var tmpName = addScriptName==='false' ? tmplName : $scope.$root.loadedTemplates[tmpl];
						var compiledDom = $compile('<ng-include onload="' + onload + '" src="\'' + tmpName + '\'"></ng-include>')(newScope, function() {
							//console.log('Compiled FROM CACHE');
						});
						elem.empty().append(compiledDom);
						return;
					} else {
						$scope.$root.loadedTemplates[tmpl] = tmplName;
					}

					$http.get(scriptName)
						.then(function(response) {
							if (showInjectionError(scriptName, response)) return;

							var code = "(function () { try { " + response.data + " } catch(e) { console.log('" + scriptName + "',e); } })();\n//# sourceURL=" + scriptName;
							injectScript(tmpl + '.js', null, code);

							//newScope.data = (typeof (data) != 'undefined') ? data : null;
							newScope.isLoaded = true;

							var compiledDom = $compile('<ng-include onload="' + onload + '" src="\'' + tmplName + '\'"></ng-include>')(newScope, function() {});
							elem.empty().append(compiledDom);

							if (typeof(callback) == 'function') callback();
						});
				}

				var showInjectionError = function(fileName, response) {
					if (response.status !== 200) {
						console.error("Can't load file " + fileName + '. Error : ' + response.statusText + '\n' + response.data);
						return true;
					}
					return false;
				}

				injectTmpl($scope.template, $scope.ctrlname, $scope.elid ? $scope.elid : $element, $scope.data, $scope.onload);
			},
			link: function($scope, $element) {
				//console.log($scope.template);
			}
		}
	})

	.directive('sldsSvgIcon', function() {
		'use strict';
		return {
			restrict: 'EA',
			scope: {
				size: '=?',
				sprite: '=?',
				icon: '=?'
			},
			replace: true,
			transclude: false,
			templateUrl: "sldsSvgIcon.tpl.html",
			link: function($scope, $element, $attrs) {
				$scope.addclasses = $attrs.class ? $attrs.class : 'slds-input__icon slds-button__icon--' + ($scope.size ? $scope.size : 'small');
				var url = $scope.$root.svgurl.replace('{0}', $scope.sprite).replace('{1}', $scope.icon);
				$element.children('use').attr('xlink:href', url);
			}
		};
	}).directive('sldsPicklist', function($translate) {
		return {
			restrict: 'AE',
			scope: {
				caption: '=?',
				source: '=?',
				placeholder: '=?',
				default: '=',
				click: '&?',
				align: '=?'
			},
			replace: true,
			transclude: false,
			templateUrl: "sldsPicklist.tpl.html",
			controller: function($scope) {
				//console.log('picklist', $scope.source);
				$scope.setSource = function(value) {
					$scope.items = [];
					for (var i = 0; i < value.length; i++) {
						var picklistItem = typeof(value[i]) == 'string' ? {	name: value[i],	label: value[i]	} : value[i];
						$scope.items.push(picklistItem);
						if (picklistItem.name === $scope.default) $scope.curItem = picklistItem;
					}	
				}

				$scope.$watch('default', function(oldValue, newValue) {
					if (oldValue == undefined) {
						$scope.curItem = null;
						//console.log($scope.source);
					}
				});

				$scope.$watch('source', function(oldValue, newValue) {
                    //console.log('$scope.source', oldValue, newValue);
					if (oldValue !== undefined) {
						$scope.setSource(oldValue);
					}
				});


			},
			link: function($scope, $element, attr) {
				if (attr.click) {
					$scope.clickAction = function(parent, $event) {
						$scope.click({
							item: parent,
							$event: $event
						});
					}
				}
			}
		};
	}).directive('sldsMessage', function() {
		return {
			restrict: 'EA',
			scope: {
				type: '=?',
				text: '=?'
			},
			replace: true,
			templateUrl: "sldsMessage.tpl.html",
			controller: function($scope, $element) {
				$scope.$watch("text", function(newValue, oldValue) {
					if (newValue == undefined || newValue == null) return;
					setTimeout(function() {
						$scope.$evalAsync(function() {
							$scope.text = null;
						});
					}, 8000);
				});
			}
		};
	}).directive('customOnChange', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				var onChangeFunc = scope.$eval(attrs.customOnChange);
				element.bind('change', function(e) {
					var file = (e.srcElement || e.target).files[0];
					onChangeFunc(file);
				});
			}
		};
	}).directive('keypressEvents', function($document, $rootScope) {
		return {
			restrict: 'A',
			link: function() {
				//console.log('linked');
				$document.bind('keydown', function(e) {
					$rootScope.$broadcast('keypress', e, e.which);
				});
			}
		}
	}).directive('codeMirror', ['$timeout', function($timeout) {
		return {
			restrict: 'E',
			replace: true,
			templateUrl: 'codemirror.tpl.html',
			scope: {
				container: '=',
			},
			controller: function($scope, $window) {
				CodeMirror.commands.jsbeautifier = function(cm) {
					$scope.jsbeautifier();
				};
				CodeMirror.keyMap.default["Ctrl-B"] = "jsbeautifier";

				$scope.$watch('container._content', function(oldValue, newValue) {
					//console.log(oldValue, newValue, $scope.editor.getValue())

					if (oldValue != $scope.editor.getValue()) {
						$scope.editor.setValue(oldValue);
						//console.log(oldValue, newValue, $scope.editor.getValue())
					}
				});

				$scope.$watch('container._cmd', function(oldValue, newValue) {
					//console.log(oldValue, newValue);
					if ($scope.container._cmd == 'init') {
							$scope.container._initState = $scope.editor.doc.historySize();	
							$scope.container.isChanged = ($scope.editor.doc.historySize().undo !== $scope.container._initState.undo);
					}
					if ($scope.container._cmd == 'jsbeauty') {
						$scope.jsbeautifier();
					}
					$scope.container._cmd = null;
						
				});

				$scope.jsbeautifier = function() {
					var the = {
						use_codemirror: true,
						beautify_in_progress: false,
						editor: $scope.editor
					};
					$window.beautify(the);
				}

			},
			link: function(scope, element, attrs) {
				var editorOpt = {
					styleActiveLine: true,
					highlightSelectionMatches: {
						showToken: /\w/,
						annotateScrollbar: true
					},
					styleActiveLine: true,
					matchBrackets: true,
					indentWithTabs: true,
					showTrailingSpace: true,
					autofocus: true,
					foldGutter: true,
					height: "auto",
					theme: 'default',
					lineWrapping: false,
					scrollbarStyle: 'native',
					foldGutter: true,
					gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
					extraKeys: {
						"Ctrl-Space": "autocomplete"
					},
					matchBrackets: true,
					autoCloseBrackets: true,
					matchTags: {
						bothTags: true
					},
					autoCloseTags: true,
					lineNumbers: true,
					tabSize: 4,
					indentUnit: 4
				};

				scope.editor = new window.CodeMirror(function(cm_el) {
					element[0].firstChild.append(cm_el);
				}, editorOpt);
				var myCodeMirror = scope.editor;

				myCodeMirror.on('change', function(cm, change) {
					if (change.origin == 'setValue') {
						detectEditorMode(scope.container._fileName, myCodeMirror);
					}
					$timeout(function() {
						scope.container._content = myCodeMirror.getValue();
						if (!scope.container._initState){
			            	scope.container._initState = scope.editor.doc.historySize();
							scope.container._initState.undo--;
						}
						
						scope.container.isChanged = (scope.editor.doc.historySize().undo !== scope.container._initState.undo);
						//console.log('isChanged', scope.container._initState, scope.editor.doc.historySize());
					}, 300);
				});

				function detectEditorMode(file, myCodeMirror) {
					var val = file,
						m, mode, spec;
					if (m = /.+\.([^.]+)$/.exec(val)) {
						var info = CodeMirror.findModeByExtension(m[1]);
						if (info) {
							mode = info.mode;
							spec = info.mime;
						}
					} else if (/\//.test(val)) {
						var info = CodeMirror.findModeByMIME(val);
						if (info) {
							mode = info.mode;
							spec = val;
						}
					} else {
						mode = spec = val;
					}
					if (mode) {
						myCodeMirror.setOption("mode", spec);
						window.CodeMirror.autoLoadMode(myCodeMirror, mode);
						//console.log(mode);
					} else {
						console.log("Could not find a mode corresponding to " + val);
					}
				}
			}
		};
	}]).service('fileReader', function($q, $log) {
		//var fileReader = function ($q, $log) {
		var onLoad = function(reader, deferred, scope) {
			return function() {
				scope.$apply(function() {
					deferred.resolve(reader.result);
				});
			};
		};
		var onError = function(reader, deferred, scope) {
			return function() {
				scope.$apply(function() {
					deferred.reject(reader.result);
				});
			};
		};
		var onProgress = function(reader, scope) {
			return function(event) {
				scope.$broadcast("fileProgress", {
					total: event.total,
					loaded: event.loaded
				});
			};
		};
		var getReader = function(deferred, scope) {
			var reader = new FileReader();
			reader.onload = onLoad(reader, deferred, scope);
			reader.onerror = onError(reader, deferred, scope);
			reader.onprogress = onProgress(reader, scope);
			return reader;
		};
		var readAsDataURL = function(file, scope) {
			var deferred = $q.defer();
			var reader = getReader(deferred, scope);
			reader.readAsBinaryString(file);
			return deferred.promise;
		};
		return {
			readAsDataUrl: readAsDataURL
		};
		//};
	});